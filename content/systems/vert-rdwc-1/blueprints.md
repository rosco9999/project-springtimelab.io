---
title: Vertical Recirculating Deep Water Culture System
subtitle: Blueprints
status: complete
---

<script src="/systems/vert-rdwc-1/cad/post-with-arms.drafting.js"
        data-scale="1400/100"
        data-width="1000" data-height="1600"
        data-origin-offset-x="400" data-origin-offset-y="100"
        data-rounding="2" data-unit="&quot;">
</script>

