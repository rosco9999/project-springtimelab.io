---
title: Vertical Recirculating Deep Water Culture System
subtitle: Construction Instructions
status: in progress
---

## Construction

You want to start with building the wooden parts, because it will take some time for the wood
stain to dry. While the stain is drying, you can work on other parts.

1. Construct the [Vertical Frame](/parts/vertical-frame-1/).

2. Construct the [Arms](/parts/arm-1/) that will hold up the growing pipes, then attach
   them to the Vertical Frame with the 1-5/8" Screws in the locations given in the
   blueprint. The blueprint shows one post of the Vertical Frame. You need to do it for
   both posts, taking care that the horizontal position of the arms is close on both posts,
   otherwise the water in the Growing Pipes will run off in one direction.

3. Stain the Vertical Frame and Arms assembly according to the instructions of the
   Wood Stain product you are using. Staining is highly recommended so the wood will not
   deteriorate in the weather.

4. Construct the [Growing Pipes](/parts/growing-pipe-1/); also drill the inflow and
   outflow holes shown in the blueprint above; [Cascading Pipes 1](/parts/cascading-pipe-1/)
   and [Cascading Pipes 2](/parts/cascading-pipe-2/) will use those. Place the Growing Pipes
   on the Arms. Make sure that as you stand in front of each Growing Pipe, the bottom
   (outflow) holes are all on the left, and the top (inflow) holes on the right, otherwise
   you won't be able to circulate the water.

5. Construct [Cascading Pipes 1](/parts/cascading-pipe-1/) and
   [Cascading Pipes 2](/parts/cascading-pipe-2/) and place them in the in and outflow
   holes of the Growing Pipes. Use the Caulking to create a watertight connection.

6. Construct the [Reservoir-Pump-Assembly](/parts/reservoir-pump-assembly) and place
   it underneath the Vertical Frame.

7. Construct the [Reservoir Inflow Pipe](/parts/reservoir-inflow-pipe-1/) and attach
   it to the Vertical Frame using a Conduit Strap as shown in the blueprint, and to
   the bottom-most Growing Pipe using Caulking to create a watertight connection.

8. Construct the [Rising Pipe Assembly](/parts/rising-pipe-assembly-1/). Screw it into
   the pump, and attach it to the Vertical Frame using a Conduit Strap as shown in the
   blueprint. Insert the end of the thin hose into the inflow hole of the top-most
   Growing Pipe, and attach it with with Cable Clamp.

Before you continue, make sure the Caulking had enough time to harden per instructions
on the Caulking tube.

