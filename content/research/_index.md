---
title: Research
comments: false
layout: single
---

Collection of interesting factoids related to hydroponic growing,
with links to the sources of those factoids.

* [Maintenance](maintenance/)
* [Methods](methods/)
* [Nutrients](nutrients/)
* [Plants](plants/)
* [Problems](problems/)
* [Substrates](substrates/)
