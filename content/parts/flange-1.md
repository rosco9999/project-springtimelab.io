---
title: "flange-1"
description: "For better seal of the cascading pipe connecting to the growing pipe"
status: complete
---

## Description

{{< page-description >}}

## Images

{{< load-photoswipe >}}
{{< gallery dir="/parts/flange-1/images/" />}}

## Overview

This is an optional part. It has two parts: a top and a bottom, which need
to be 3D-printed. Once printed, they slide into each other from inside
and outside the drainage hole of the growing pipe, holding the exiting
cascading pipe in place.

## Bill of materials

* Food-safe 3D printing filament

## Required tools

* 3D Printer
* PC running [OpenSCAD](http://www.openscad.org/)
* PC running a slicing program suitable for your 3D printer, such as
  [Slic3r](https://slic3r.org/)

## Construction

* Download the OpenSCAD models:

  * [flange-top.scad](/parts/flange-1/cad/flange-top.scad)
  * [flange-bottom.scad](/parts/flange-1/cad/flange-bottom.scad)

* Alternatively, if you are more technically inclined and have ``git`` installed, clone
  the entire Project Springtime website from
  [here](https://gitlab.com/project-springtime/project-springtime.gitlab.io) and look
  for the .scad files in ``content/parts/flange-1/cad``.
* In OpenSCAD, open this .scad file you want and and select "Design/Render (F6)".
* In OpenSCAD, export as STL.
* In your slicer, place the previously exported STL file.
* In your slicer, you can probably use default parameters for your printer. This is not
  a part that has to be printed particularly precisely. However, you may not want to use
  the fastest printing mode, as some aspects of the part are fairly thin, and printing fast
  may not add enough material to make them sturdy enough.
* In your slicer, export the G-Code and print the G-Code.
