---
title: "pot-holder-1"
description: "Helps keep the net pot upright."
status: complete
---

## Description

{{< page-description >}}

## Images

{{< load-photoswipe >}}
{{< gallery dir="/parts/pot-holder-1/images/" />}}

## Overview

This entirely optional 3D-printed part keeps the net pot from "rolling" on the round
growing pipe. This may otherwise happen if the plant is tall and top-heavy.

## Bill of materials

* 3D printing filament

## Required tools

* 3D Printer
* PC running [OpenSCAD](http://www.openscad.org/)
* PC running a slicing program suitable for your 3D printer, such as
  [Slic3r](https://slic3r.org/)

## Construction

* Download the OpenSCAD model from [here](/parts/pot-holder-1/cad/pot-holder.scad)
* Alternatively, if you are more technically inclined and have ``git`` installed, clone
  the entire Project Springtime website from
  [here](https://gitlab.com/project-springtime/project-springtime.gitlab.io) and look
  for file ``content/parts/pot-holder-1/cad/pot-holder.scad``.
* In OpenSCAD, open this file and select "Design/Render (F6)".
* In OpenSCAD, export as STL.
* In your slicer, place the previously exported STL file.
* In your slicer, export the G-Code and print the G-Code.
