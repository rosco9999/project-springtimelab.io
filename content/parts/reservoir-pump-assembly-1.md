---
title: "reservoir-pump-assembly-1"
description: "Assembly of reservoir, pump and rising pipe"
status: started
---

## Description

{{< page-description >}}

## Images

{{< load-photoswipe >}}
{{< gallery dir="/parts/reservoir-pump-assembly-1/images/" />}}
