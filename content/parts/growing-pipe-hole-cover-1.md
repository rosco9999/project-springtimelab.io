---
title: "growing-pipe-hole-cover-1"
description: "Covers currently unused holes in a horizontal growing pipe."
status: complete
---

## Description

{{< page-description >}}

## Images

{{< load-photoswipe >}}
{{< gallery dir="/parts/growing-pipe-hole-cover-1/images/" />}}

## Overview

This is an entirely optional part, and you can solve the problem differently with some
kind of flap. This is a 3D-printed part that covers unused holes in a horizontal growing
pipe and thus prevents algae from growing.

We provide two models:

* The normal hole cover
* The same hole cover, but with straight trim on one side, as if cut off with a saw.
  This is useful if for some reason, your to-be-covered hole is too close to the growing
  pipe's end cap.

## Bill of materials

* 3D printing filament

## Required tools

* 3D Printer
* PC running [OpenSCAD](http://www.openscad.org/)
* PC running a slicing program suitable for your 3D printer, such as
  [Slic3r](https://slic3r.org/)

## Construction

* Download the OpenSCAD model:
  * from [here](/parts/growing-pipe-hole-cover-1/cad/growing-pipe-hole-cover.scad) for
    the default, and
  * from [here](/parts/growing-pipe-hole-cover-1/cad/growing-pipe-hole-cover-with-cutoff.scad) for
    the version that is cut off. You may need to edit this file to change the location of
    the cutoff to match your requirements
* Alternatively, if you are more technically inclined and have ``git`` installed, clone
  the entire Project Springtime website from
  [here](https://gitlab.com/project-springtime/project-springtime.gitlab.io) and look
  for files ``content/parts/growing-pipe-hole-cover-1/cad/growing-pipe-hole-cover.scad``
  or ``content/parts/growing-pipe-hole-cover-1/cad/growing-pipe-hole-cover-with-cutoff.scad``.
* In OpenSCAD, open this file and select "Design/Render (F6)".
* In OpenSCAD, export as STL.
* In your slicer, place the previously exported STL file.
* This part has an overhang. Depending on your printer and the default settings in your
  slicer, you may or may not need to provide extra parameters (e.g. for extra support) to
  be successful printing this part.
* In your slicer, export the G-Code and print the G-Code.
* Use a file or sharp knife to beautify your part if needed.
