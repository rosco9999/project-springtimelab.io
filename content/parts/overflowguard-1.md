---
title: "overflowguard-1"
description: "Prevents roots from clogging and overflowing draining cascading pipes."
status: complete
---

## Description

{{< page-description >}}

## Images

{{< load-photoswipe >}}
{{< gallery dir="/parts/overflowguard-1/images/" />}}

## Overview

This entirely optional 3D-printed part can be used to keep long roots away from the
inflow of the cascading pipes that drain the water from the growing pipe. This makes
it less likely that the roots clog the system and the water overflows instead of
draining.

## Bill of materials

* 3D printing filament

## Required tools

* 3D Printer
* PC running [OpenSCAD](http://www.openscad.org/)
* PC running a slicing program suitable for your 3D printer, such as
  [Slic3r](https://slic3r.org/)

## Construction

* Download the OpenSCAD model from [here](/parts/overflowguard-1/cad/overflowguard.scad)
* Alternatively, if you are more technically inclined and have ``git`` installed, clone
  the entire Project Springtime website from
  [here](https://gitlab.com/project-springtime/project-springtime.gitlab.io) and look
  for file ``content/parts/overflowguard-1/cad/overflowguard.scad``.
* In OpenSCAD, open this file and select "Design/Render (F6)".
* In OpenSCAD, export as STL.
* In your slicer, place the previously exported STL file.
* In your slicer, export the G-Code and print the G-Code.
