---
title: "plant-holder-1"
description: "Helps taller and heavier plants stand up and not topple over."
status: complete
---

## Description

{{< page-description >}}

## Images

{{< load-photoswipe >}}
{{< gallery dir="/parts/plant-holder-1/images/" />}}

## Overview

This entirely optional 3D-printed part (which comes in two pieces) is used to help
taller, and top-heavy plants to not topple over. This may happen because the growing
cubes are not particularly stable in the net pots, and the plant may not have managed
to hold on to the net pot very well.

## Bill of materials

* 3D printing filament

## Required tools

* 3D Printer
* PC running [OpenSCAD](http://www.openscad.org/)
* PC running a slicing program suitable for your 3D printer, such as
  [Slic3r](https://slic3r.org/)

## Construction

* Download the OpenSCAD model from [here](/parts/plant-holder-1/cad/plant-holder.scad)
* Alternatively, if you are more technically inclined and have ``git`` installed, clone
  the entire Project Springtime website from
  [here](https://gitlab.com/project-springtime/project-springtime.gitlab.io) and look
  for file ``content/parts/plant-holder-1/cad/plant-holder.scad``.
* In OpenSCAD, open this file and select "Design/Render (F6)".
* In OpenSCAD, export as STL.
* In your slicer, place the previously exported STL file.
* In your slicer, export the G-Code and print the G-Code.
