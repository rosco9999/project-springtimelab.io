---
title: "cubeholder-1"
description: "Keeps small sections of the growing cube material from topping over in the germination tray"
status: complete
---

## Description

{{< page-description >}}

## Images

{{< load-photoswipe >}}
{{< gallery dir="/parts/cubeholder-1/images/" />}}

## Overview

This is an entirely optional part. It is convenient if you germinate small batches of
seeds, and break the cube material into relatively small pieces. These small pieces
tend to topple over in the germination tray, drowning the plant.

This part, in some variations, prevents the growing cube(s) from toppling over.

## Bill of materials

* Food-safe 3D printing filament

## Required tools

* 3D Printer
* PC running [OpenSCAD](http://www.openscad.org/)
* PC running a slicing program suitable for your 3D printer, such as
  [Slic3r](https://slic3r.org/)

## Construction

* Download one of the OpenSCAD models, depending on the size and shape you want:

  * [cubeholder-1x3-1x3.scad](/parts/cubeholder-1/cad/cubeholder-1x3-1x3.scad)
  * [cubeholder-2x1.scad](/parts/cubeholder-1/cad/cubeholder-2x1.scad)
  * [cubeholder-2x2-2x1.scad](/parts/cubeholder-1/cad/cubeholder-2x2-2x1.scad)
  * [cubeholder-2x2.scad](/parts/cubeholder-1/cad/cubeholder-2x2.scad)
  * [cubeholder-3x2-3x1.scad](/parts/cubeholder-1/cad/cubeholder-3x2-3x1.scad)
  * [cubeholder-3x2.scad](/parts/cubeholder-1/cad/cubeholder-3x2.scad)
  * [cubeholder-4x2-4x1.scad](/parts/cubeholder-1/cad/cubeholder-4x2-4x1.scad)

* Alternatively, if you are more technically inclined and have ``git`` installed, clone
  the entire Project Springtime website from
  [here](https://gitlab.com/project-springtime/project-springtime.gitlab.io) and look
  for the .scad files in ``content/parts/cubeholder-1/cad``.
* In OpenSCAD, open this .scad file you want and and select "Design/Render (F6)".
* In OpenSCAD, export as STL.
* In your slicer, place the previously exported STL file.
* In your slicer, you can probably use default parameters for your printer. This is not
  a part that has to be printed particularly precisely. However, you may not want to use
  the fastest printing mode, as some aspects of the part are fairly thin, and printing fast
  may not add enough material to make them sturdy enough.
* In your slicer, export the G-Code and print the G-Code.
