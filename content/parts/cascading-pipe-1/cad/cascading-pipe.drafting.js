class ElbowPart extends DjsElement {
    constructor( start, angle ) {
        super();
        this.start = start;
        this.angle = angle;
    }

    convertToSvg( params ) {
        var p  = this.start;
        var outer  = 1.125;
        var sleeve = 0.75;
        var r      = outer;

        var delegate = new DjsClosedPathPart( p )
                .lineTo( p.plus( outer,          0              ).rotateByAngle( p, this.angle ))
                .lineTo( p.plus( outer,          sleeve         ).rotateByAngle( p, this.angle ))
                .lineTo( p.plus( outer + sleeve, sleeve         ).rotateByAngle( p, this.angle ))
                .lineTo( p.plus( outer + sleeve, sleeve + outer ).rotateByAngle( p, this.angle ))
                .lineTo( p.plus( outer,          sleeve + outer ).rotateByAngle( p, this.angle ))
                .arcTo(  p.plus( 0,              sleeve         ).rotateByAngle( p, this.angle ), r, 0 );

        return delegate.convertToSvg( params );
    }
}
djsRender( document.currentScript, function( params ) {
    var p  = new DjsPoint( 2, 0 );
    var w  = 0.85;

    var p2 = p.plus( 1.125/2 + 0.75, 2 + 0.75 + 1.125/2 );
    var p3 = p2.plus( 10 + 0.75 + 1.125/2, 1.125/2 + 0.75 );

    return new DjsDiagram(
        [
            new DjsRectanglePart( p.plus( -w/2, 0 ), p.plus( w/2, 2 ) ),
            new ElbowPart( p.plus( -1.125/2, 2 ), 0 ),

            new DjsRectanglePart( p2.plus( 0, - w/2 ), p2.plus( 10, w/2 ) ),
            // new ElbowPart( p2.plus( 10 + 1.125 + 0.75, 1.125/2 + 0.75 ), 180 ),
            new ElbowPart( p3.plus( 1.125/2, 0 ), 180 ),

            new DjsRectanglePart( p3.plus( -w/2, 0 ), p3.plus( w/2, 2 ) ),

            new DjsPartLabel( p2.plus( 5, w/2 ), "Cascading-pipe-1" )
        ],
        params );
} );
