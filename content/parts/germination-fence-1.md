---
title: "germination-fence-1"
description: "Visual separator for germinating seeds."
status: complete
---

## Description

{{< page-description >}}

## Images

{{< load-photoswipe >}}
{{< gallery dir="/parts/germination-fence-1/images/" />}}

## Overview

This is an entirely optional part. We just noticed that when germinating different
types of seeds, perhaps starting at different times, in the same germination tray, it
can be difficult to keep them separate and remember which is which.

So we created this germination fence, which just sits in the tray as a visual separator
between the different batches of germinating seeds.

## Bill of materials

* Food-safe 3D printing filament

## Required tools

* 3D Printer
* PC running [OpenSCAD](http://www.openscad.org/)
* PC running a slicing program suitable for your 3D printer, such as
  [Slic3r](https://slic3r.org/)

## Construction

* Download the OpenSCAD model from [here](/parts/germination-fence-1/cad/germination-fence.scad).
  Alternatively, if you are more technically inclined and have ``git`` installed, clone
  the entire Project Springtime website from
  [here](https://gitlab.com/project-springtime/project-springtime.gitlab.io) and look
  for file ``content/parts/germination-fence-1/cad/germination-fence.scad``.
* In OpenSCAD, open this file and select "Design/Render (F6)".
* In OpenSCAD, export as STL.
* In your slicer, place the previously exported STL file.
* In your slicer, you can probably use default parameters for your printer. This is not
  a part that has to be printed particularly precisely. However, you may not want to use
  the fastest printing mode, as some aspects of the part are fairly thin, and printing fast
  may not add enough material to make them sturdy enough.
* In your slicer, export the G-Code and print the G-Code.
